/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class LfsStringParser {
    static Map<String, String> encodings;
    static
    {
        encodings = new HashMap<>();
        encodings.put("L", "cp1252");
        encodings.put("G", "cp1253");
        encodings.put("C", "cp1251");
        encodings.put("J", "MS932");
        encodings.put("E", "cp1250");
        encodings.put("T", "cp1254");
        encodings.put("B", "cp1257");
        encodings.put("H", "x-windows-950");
        encodings.put("S", "CP936");
        encodings.put("K", "MS949");
    }

    private static String decodeBuffer(ByteBuffer buff, String charset) {
        String ret_str = "";
        try {
            ret_str = new String(buff.array(), 0, buff.position(), charset);
            // Log.v("decoded", new String(buff.array(), 0, buf_length, charset));
            buff.clear();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return ret_str;
    }


    public static String parseString (String s, boolean colorise) {
        String ret_str = "";
        String current_charset = "cp1252";
        int i = 0;
        boolean is_colored = false;
        ByteBuffer buff = ByteBuffer.allocate(512);
        for (; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '^') {
                if (encodings.keySet().contains(Character.toString(s.charAt(i + 1)))) {
                    if (i > 0) {
                        ret_str += decodeBuffer(buff, current_charset);
                    }
                    current_charset = encodings.get(Character.toString(s.charAt(i + 1)));
                }
                else if (s.charAt(i + 1) == '^') {
                    buff.put((byte)'^');
                }
                else if (Character.isDigit(s.charAt(i + 1))) {
                    if (colorise) {
                        if (is_colored) {
                            buff.put("</font>".getBytes());
                        }
                        is_colored = true;
                        switch (s.charAt(i + 1)) {
                            case '0':
                                buff.put("<font color=#444444>".getBytes());
                                break;
                            case '1':
                                buff.put("<font color=red>".getBytes());
                                break;
                            case '2':
                                buff.put("<font color=#00BB00>".getBytes());
                                break;
                            case '3':
                                buff.put("<font color=#BBBB00>".getBytes());
                                break;
                            case '4':
                                buff.put("<font color=blue>".getBytes());
                                break;
                            case '5':
                                buff.put("<font color=Fuchsia>".getBytes());
                                break;
                            case '6':
                                buff.put("<font color=#00AAAA>".getBytes());
                                break;
                            case '7':
                                buff.put("<font color=white>".getBytes());
                                break;
                            case '8': // no idea if it is used at all in lfs
                                buff.put("<font color=gray>".getBytes());
                                break;
                            case '9':
                                buff.put("<font color=gray>".getBytes());
                                break;
                        }
                    }
                }
                else {
                    Log.v("wtf got (ignoring ):", Character.toString(s.charAt(i + 1)));
                }
                ++i;
            }
            else {
                buff.put((byte) c);
            }
        }
        if (buff.position() > 0) {
            ret_str += decodeBuffer(buff, current_charset);
        }
        if (is_colored) {
            ret_str += "</font>";
        }
        return ret_str;
    }
}
