/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import java.util.List;

public class ServerInfo {
    public String serverName;
    public String serverNameColored;
    public String track;
    public List<String> onlineUsers;

    public List<String> carsAllowed;

    ServerInfo (String server_name,
                String server_name_colored,
                String track_,
                List<String> online_users,
                List<String> cars_allowed) {
        serverName = server_name;
        serverNameColored = server_name_colored;
        track = track_;
        onlineUsers = online_users;
        carsAllowed = cars_allowed;
    }
}
