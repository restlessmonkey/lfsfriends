/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VersionMigrate {
    public static void migrateFrom7 (SharedPreferences sharedpreferences) {
        String team = sharedpreferences.getString("team", "");
        if (team.length() > 0) {
            List<String> teammates = new ArrayList<>();
            Utils.populateListFromJson(teammates, sharedpreferences.getString(AppSettings.teammates, ""));

            SharedPreferences.Editor editor = sharedpreferences.edit();

            JSONObject obj = new JSONObject();
            JSONArray arr = new JSONArray();

            for (String each_teammate : teammates) {
                arr.put(each_teammate);
            }
            try {
                obj.put(team, arr);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            editor.remove("team");

            editor.putString(AppSettings.teammates, obj.toString());
            editor.commit();
        }
    }
}
