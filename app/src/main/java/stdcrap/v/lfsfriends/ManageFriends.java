/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;


public class ManageFriends extends Activity {
    SharedPreferences sharedpreferences;

    private List<String> friends;

    private ArrayAdapter<String> arrayAdapter;
    ListView lv;

    public ManageFriends () {
        friends = new ArrayList< >();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_friends);

        lv = (ListView) findViewById(R.id.listViewFriendsList);
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        arrayAdapter = new ArrayAdapter< >(this,
                                           android.R.layout.simple_list_item_multiple_choice,
                                           friends);
        arrayAdapter.notifyDataSetChanged();

        lv.setAdapter(arrayAdapter);
    }

    private void reloadFriends() {
        sharedpreferences = getSharedPreferences(AppSettings.preferencesName, Context.MODE_PRIVATE);

        Utils.populateListFromJson(friends, sharedpreferences.getString(AppSettings.friends, ""));
        arrayAdapter.notifyDataSetChanged();
    }

    private void saveFriends () {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        Log.v("saved friends", Utils.listToJsonString(friends));

        editor.putString(AppSettings.friends, Utils.listToJsonString(friends));
        editor.commit();
    }

    protected void onResume() {
        super.onResume();
        reloadFriends();
    }

    protected void onPause () {
        super.onPause();
        saveFriends();
    }

    public void deleteRequested (View view) {
        SparseBooleanArray selected = lv.getCheckedItemPositions();

        for(int i = friends.size() - 1; i >= 0; i--){
            if(selected.get(i)){
                friends.remove(i);
            }
        }
        saveFriends();
        reloadFriends();
        lv.clearChoices();
    }
}
