/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.zip.InflaterInputStream;

public class GetPubstatTask extends AsyncTask<String, Integer, String>
{
    private PubstatReceiver callerRef;
    private String requestUrl;
    private boolean wrongRequest;

    public GetPubstatTask (PubstatReceiver g, String idk) {
        wrongRequest = false;
        requestUrl = String.format("http://www.lfsworld.net/pubstat/get_stat2.php?version=1.5&idk=%s&s=1&c=2", idk);
        if (idk.length() < 1) {
            wrongRequest = true;
        }
        callerRef = g;
    }

    public static byte[] getResponseFromUrl(String url) {
        byte[] xml = null;
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            xml = EntityUtils.toByteArray(httpEntity);
            if (BuildConfig.DEBUG) {
                Log.v("got bytes from lfsworld", Integer.toString(xml.length));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return xml;
    }

    public static String decompress(byte[] bytes) {
        InputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[8192];
            int len;
            while((len = in.read(buffer))>0)
                baos.write(buffer, 0, len);
            return new String(baos.toByteArray(), "US-ASCII");
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @Override
    protected String doInBackground(String... params)
    {
        if (wrongRequest) {
            return "";
        }
        byte[] resp = getResponseFromUrl(requestUrl + params[0]);
        Log.v("response length", String.valueOf(resp.length));

        return decompress(resp);
    }

    @Override
    protected void onPostExecute(String result) {
        callerRef.pubstatGot(result);
    }
}
