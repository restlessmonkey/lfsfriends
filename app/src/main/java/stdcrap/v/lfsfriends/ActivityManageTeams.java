/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ActivityManageTeams extends Activity
        implements PubstatReceiver {
    SharedPreferences sharedpreferences;

    private String idk;

    private ArrayList<HashMap<String, Object>> teammates;
    private SimpleAdapter arrayAdapterTeams;
    ListView lvt;
    public ActivityManageTeams () {
        teammates = new ArrayList< >();
        teammates = new ArrayList< >();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_teams);

        lvt = (ListView) findViewById(R.id.listViewTeamsList);

        arrayAdapterTeams = new SimpleAdapter(this, teammates,
                android.R.layout.simple_list_item_multiple_choice,
                new String[] { "line1","line2" },
                new int[] {android.R.id.text1, android.R.id.text2});
        arrayAdapterTeams.setViewBinder(new SimpleAdapter.ViewBinder() {
            public boolean setViewValue(View view, Object data, String textRepresentation) {
                if (data instanceof List) {
                    List l = (List) data;
                    ((TextView) view).setText(l.size() + " members");
                }
                else {
                    //String users = String.valueOf(data);
                    ((TextView) view).setText(String.valueOf((data)));
                }
                return true;
            }
        });
        lvt.setAdapter(arrayAdapterTeams);
        lvt.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }

    public void onResume () {
        super.onResume();
        reloadTeammates();
    }

    private void reloadTeammates() {
        sharedpreferences = getSharedPreferences(AppSettings.preferencesName, Context.MODE_PRIVATE);
        idk = sharedpreferences.getString(AppSettings.idk, "");

        teammates.clear();
        try {
            JSONObject teammates_ = new JSONObject(sharedpreferences.getString(AppSettings.teammates, ""));
            Iterator<?> keys = teammates_.keys();

            while( keys.hasNext() ){
                String team_name = (String)keys.next();
                List<String> teammates_i = new ArrayList<>();
                Utils.populateListFromJson(teammates_i, teammates_.getJSONArray(team_name));

                HashMap<String,Object> item = new HashMap<>();
                item.put("line1", team_name);
                item.put("line2", teammates_i);
                teammates.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        arrayAdapterTeams.notifyDataSetChanged();

        String upd_time = sharedpreferences.getString(AppSettings.teammatesUpdateTime, "update failed");
        TextView tv = (TextView) findViewById(R.id.teammatesUpdateTime);
        tv.setText(getString(R.string.updated_at) + upd_time);
    }

    public void teamAddRequested (View view) {
        final EditText input = new EditText(this);

        new AlertDialog.Builder(ActivityManageTeams.this)
                .setTitle(R.string.add_team_dialog_title)
                .setMessage(R.string.add_team_dialog_message)
                .setView(input)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value = input.getText().toString();
                        if (!haveTeam(value)) {
                            HashMap<String,Object> item = new HashMap<>();
                            item.put("line1", value);
                            item.put("line2", new ArrayList<String>());
                            teammates.add(item);
                            updateTeams();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show()
                .getWindow().setSoftInputMode (WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    private boolean haveTeam (String team_name) {
        for(HashMap<String, Object> each_team : teammates) {
            if (((String) each_team.get("line1")).equals(team_name)) {
                return true;
            }
        }
        return false;
    }

    public void deleteTeamRequested (View view) {
        SparseBooleanArray selected = lvt.getCheckedItemPositions();

        for(int i = teammates.size() - 1; i >= 0; i--){
            if(selected.get(i)){
                teammates.remove(i);
            }
        }
        updateTeams();
        lvt.clearChoices();
    }

    private void updateTeams() {
        new GetPubstatTask(this, idk).execute("&action=teams");
    }

    public void updateTeamsRequested (View view) {
        updateTeams();
    }

    private void getListOfTeamMates (String pubstat) throws JSONException {
        JSONArray teams = new JSONArray(pubstat);
        for (int i = 0; i < teams.length(); i++) {
            JSONObject team = teams.getJSONObject(i);
            String team_name = team.getString("team");

            // finding team
            List<String> l = null;

            for(HashMap<String, Object> each_team : teammates) {
                if (((String) each_team.get("line1")).equals(team_name)) {
                    l = (List<String>) each_team.get("line2");
                    l.clear();
                    break;
                }
            }

            if (l != null) {
                JSONArray team_members = team.getJSONArray("members");
                for (int j = 0; j < team_members.length(); j++) {
                    String racer = team_members.getString(j);
                    l.add(racer);
                }
            }
        }
    }

    public void pubstatGot(String pubstat) {
        try {
            getListOfTeamMates(pubstat);
        } catch (JSONException e) {
            e.printStackTrace();
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(AppSettings.teammatesUpdateTime, "update failed");
            editor.commit();
            return;
        }

        SharedPreferences.Editor editor = sharedpreferences.edit();

        JSONObject obj = new JSONObject();
        for(HashMap<String, Object> each_team : teammates) {
            JSONArray arr = new JSONArray();

            for (String each_teammate : (List<String>) each_team.get("line2")) {
                arr.put(each_teammate);
            }
            try {
                obj.put((String) each_team.get("line1"), arr);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        String ddd = sdf.format(new Date());
        editor.putString(AppSettings.teammatesUpdateTime, ddd);
        editor.putString(AppSettings.teammates, obj.toString());
        editor.commit();

        reloadTeammates();
    }
}
