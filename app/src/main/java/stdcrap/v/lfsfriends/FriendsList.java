/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class FriendsList extends Activity
                         implements PubstatReceiver,
                                    SwipeRefreshLayout.OnRefreshListener {
    private ArrayList<HashMap<String, Object>> friendsOnline;
    private HashMap<String,ServerInfo> serverInfos;
    SimpleAdapter friendsAdapter;
    private List<String> favoriteServers;

    SharedPreferences sharedpreferences;

    private String idk;
    private List<String> friends;
    private List<String> teammates;

    SwipeRefreshLayout swipeLayout;

    public FriendsList () {
        friendsOnline = new ArrayList< >();
        friends = new ArrayList< >();
        teammates = new ArrayList< >();
        favoriteServers = new ArrayList< >();
        serverInfos = new HashMap<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_friends_list);

        ListView lv = (ListView) findViewById(R.id.listView);

        friendsAdapter = new SimpleAdapter(this, friendsOnline,
                android.R.layout.two_line_list_item ,
                new String[] { "line1","line2" },
                new int[] {android.R.id.text1, android.R.id.text2});
        friendsAdapter.setViewBinder(new SimpleAdapter.ViewBinder() {
                                         public boolean setViewValue(View view, Object data, String textRepresentation) {
                                             if (data instanceof ServerInfo) {
                                                 ServerInfo si = (ServerInfo) data;
                                                 Spanned span = Html.fromHtml(si.serverNameColored
                                                                              + " (" +si.track + " " + Integer.toString(si.onlineUsers.size()) + " conns)");
                                                 ((TextView) view).setText(span);
                                             }
                                             else {
                                                 ((TextView) view).setText(String.valueOf((data)));
                                             }
                                             return true;
                                         }
                                     }
        );

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
            {
                Intent i = new Intent(getApplicationContext(), ActivityServerInfo.class);
                Globals.si = (ServerInfo) friendsOnline.get(position).get("line2");
                startActivity(i);
            }
        });
        lv.setAdapter(friendsAdapter);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        swipeLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        sharedpreferences = getSharedPreferences(AppSettings.preferencesName, Context.MODE_PRIVATE);
        VersionMigrate.migrateFrom7(sharedpreferences);

        idk = sharedpreferences.getString(AppSettings.idk, "");
        Utils.populateListFromJson(friends, sharedpreferences.getString(AppSettings.friends, ""));

        teammates.clear();
        try {
            JSONObject teammates_ = new JSONObject(sharedpreferences.getString(AppSettings.teammates, ""));
            Iterator<?> keys = teammates_.keys();

            while( keys.hasNext() ) {
                String teamname = (String) keys.next();
                List<String> teammates_i = new ArrayList<>();
                Utils.populateListFromJson(teammates_i, teammates_.getJSONArray(teamname));
                teammates.addAll(teammates_i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("ManageFriends::reloadFriends()", "failed parsing json: " + sharedpreferences.getString(AppSettings.teammates, ""));
        }

        Utils.populateListFromJson(favoriteServers, sharedpreferences.getString(AppSettings.servers, ""));

        doUpdate();
    }

    private ArrayList<HashMap<String,Object>> getListOfOnlineFriends (String pubstat) {
        ArrayList<HashMap<String,Object>> retList = new ArrayList< >();

        try {
            JSONArray servers = new JSONArray(pubstat);
            for (int i = 0; i < servers.length(); i++) {
                JSONObject server = servers.getJSONObject(i);
                boolean server_added = false;
                if (server.getInt("nrracers") > 0) {
                    String server_name_nocolor = LfsStringParser.parseString(server.getString("hostname"), false);
                    JSONArray racers = server.getJSONArray("racers");
                    for (int j = 0; j < racers.length(); j++) {
                        String racer = racers.getString(j);
                        if(Utils.containsCaseInsensitive(racer, friends)
                           || Utils.containsCaseInsensitive(racer, teammates)) {
                            HashMap<String,Object> item = new HashMap<>();
                            item.put("line1", racer);

                            if (!server_added) {
                                ServerInfo inf = LfsTrackParser.getServerInfo(server);
                                serverInfos.put(server_name_nocolor, inf);
                                item.put("line2", inf);
                            }
                            else {
                                item.put("line2", serverInfos.get(server_name_nocolor));
                            }

                            retList.add(item);
                            server_added = true;
                        }
                    }
                    if (favoriteServers.contains(server_name_nocolor) && (!server_added)) {
                        HashMap<String,Object> item = new HashMap<>();
                        item.put("line1", "");
                        ServerInfo inf = LfsTrackParser.getServerInfo(server);
                        serverInfos.put(server_name_nocolor, inf);
                        item.put("line2", inf);
                        retList.add(item);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (pubstat.length() == 0) {
                HashMap<String, Object> item = new HashMap< >();
                item.put("line1", "could not get pubstat from lfsworld");
                item.put("line2", "check if settings are fine");
                retList.add(item);
            }
            else if (pubstat.length() > 101) {
                HashMap<String, Object> item = new HashMap<>();
                item.put("line1", "could not get pubstat from lfsworld");
                item.put("line2", "check if settings are fine");
                retList.add(item);
            }
            else {
                HashMap<String, Object> item = new HashMap<>();
                item.put("line1", "lfsw answer: " + pubstat);
                item.put("line2", "");
                retList.add(item);
            }
        }
//        if (pubstat.length() > 101) {
//            Log.v("FriendsList::getListOfOnlineFriends() pubstat_string[0..100]: ", pubstat.substring(0, 100));
//        }
//        else {
//            Log.v("FriendsList::getListOfOnlineFriends() pubstat_string: ", pubstat);
//        }

        return retList;
    }

    public void pubstatGot (String pubstat) {
        friendsOnline.clear();
        friendsOnline.addAll(getListOfOnlineFriends(pubstat));

        TextView tv = (TextView) findViewById(R.id.updateTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        String ddd = sdf.format(new Date());
        tv.setText(getString(R.string.updated_at) + ddd);

        friendsAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_friends_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, UserSettings.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_manage_friends) {
            Intent intent = new Intent(this, ManageFriends.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_manage_teams) {
            Intent intent = new Intent(this, ActivityManageTeams.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_legal_notice) {
            Intent intent = new Intent(this, LegalNotice.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_add_friend) {
            addFriend();
            return true;
        }
        if (id == R.id.action_manage_servers) {
            Intent intent = new Intent(this, ActivityManageServers.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void saveFriends () {
        Editor editor = sharedpreferences.edit();

        editor.putString(AppSettings.friends, Utils.listToJsonString(friends));
        editor.commit();
    }

    @Override
    public void onPause () {
        super.onPause();
        saveFriends();
    }

    public void addFriend() {
        final EditText input = new EditText(this);

        new AlertDialog.Builder(FriendsList.this)
                .setTitle(R.string.add_friend_dialog_title)
                .setMessage(R.string.add_friend_dialog_message)
                .setView(input)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value = input.getText().toString();
                        if (!Utils.containsCaseInsensitive(value, friends)) {
                            friends.add(value);
                            saveFriends();
                            doUpdate();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show()
                .getWindow().setSoftInputMode (WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public void doUpdate () {
        if ((friends.size() + teammates.size() + favoriteServers.size()) > 0) {
            new GetPubstatTask(this, idk).execute("&action=hosts");
            return;
        }
        else if (friendsOnline.size() > 0) {
            friendsOnline.clear();
            friendsAdapter.notifyDataSetChanged();
        }
        TextView tv = (TextView) findViewById(R.id.updateTime);
        tv.setText(getString(R.string.updated_at) + getString(R.string.nothing_to_update));
    }

    public void onRefresh () {
        doUpdate();
        swipeLayout.setRefreshing(false);
    }
}
