/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class UserSettings extends Activity {
    SharedPreferences sharedpreferences;
    private String idk;
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);
    }

    protected void onResume() {
        super.onResume();
        sharedpreferences = getSharedPreferences(AppSettings.preferencesName, Context.MODE_PRIVATE);
        idk = sharedpreferences.getString(AppSettings.idk, "");

        EditText textView = (EditText)findViewById(R.id.editText);
        textView.setText(idk);
    }

    public void okPressed (View view) {
        finish();
    }

    protected void onPause () {
        super.onPause();
        SharedPreferences.Editor editor = sharedpreferences.edit();
        EditText textView = (EditText)findViewById(R.id.editText);
        editor.putString(AppSettings.idk, textView.getText().toString());
        editor.commit();
    }
}
