/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityServerInfo extends Activity {
    private ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Spanned span = Html.fromHtml(Globals.si.serverNameColored);
        setTitle(span);

        setContentView(R.layout.activity_server_info);

        ListView lv = (ListView) findViewById(R.id.listGuysOnline);

        arrayAdapter = new ArrayAdapter< >(this,
                android.R.layout.simple_list_item_1,
                Globals.si.onlineUsers );
        lv.setAdapter(arrayAdapter);
    }

    @Override
    protected void onResume () {
        super.onResume();
        TextView tv = (TextView) findViewById(R.id.textServerInfo);

        String cars_string = "";
        for (String car : Globals.si.carsAllowed) {
            cars_string += car + " ";
        }
        tv.setText("Track: " + Globals.si.track + "\nCars: " + cars_string);
    }
}
