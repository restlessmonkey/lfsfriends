/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LfsTrackParser {
    private static String[] tracks = {"BL", "SO", "FE", "AU", "KY", "WE", "AS"};
    private static String[] cars = {"XFG", "XRG", "XRT", "RB4", "FXO",
                                    "LX4", "LX6", "MRT", "UF1", "RAC",
                                    "FZ5", "FOX", "XFR", "UFR", "FO8",
                                    "FXR", "XRR", "FZR", "BF1"};

    public static String getTrackName(String tcrm) {
        String retStr;
        retStr = tracks[tcrm.charAt(0)]
               + Integer.toString((int)tcrm.charAt(1) + 1);
        retStr += (tcrm.charAt(2) == 1) ? "R" : "";
        return retStr;
    }


    public static ServerInfo getServerInfo (JSONObject srv) {
        ServerInfo inf = null;
        try {
            String server_name = LfsStringParser.parseString(srv.getString("hostname"), true);
            String server_name_nocolor = LfsStringParser.parseString(srv.getString("hostname"), false);
            String track = LfsTrackParser.getTrackName(srv.getString("tcrm"));

            JSONArray racers = srv.getJSONArray("racers");
            List<String> guys_on_srv = new ArrayList<>();
            for (int j = 0; j < racers.length(); j++) {
                guys_on_srv.add(racers.getString(j));
            }

            ArrayList<String> cs = new ArrayList<>();
            int c = srv.getInt("cars");
            for (int i = 0; i < cars.length; ++i)
            {
                if (((c >> i) & 1) == 1) {
                    cs.add(cars[i]);
                }
            }

            inf = new ServerInfo(server_name_nocolor,
                server_name,
                track,
                guys_on_srv,
                cs);
        } catch (JSONException e) {
                e.printStackTrace();
        }
        return inf;
    }

}
