/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class ActivityManageServers extends Activity {
    SharedPreferences sharedpreferences;

    private List<String> servers;

    private ArrayAdapter<String> arrayAdapter;
    ListView lv;

    public ActivityManageServers() {
        servers = new ArrayList< >();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_servers);

        lv = (ListView) findViewById(R.id.listViewServersList);
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        arrayAdapter = new ArrayAdapter< >(this,
                android.R.layout.simple_list_item_multiple_choice,
                servers);
        arrayAdapter.notifyDataSetChanged();

        lv.setAdapter(arrayAdapter);
    }

    private void reloadServers() {
        sharedpreferences = getSharedPreferences(AppSettings.preferencesName, Context.MODE_PRIVATE);

        Utils.populateListFromJson(servers, sharedpreferences.getString(AppSettings.servers, ""));
        arrayAdapter.notifyDataSetChanged();
    }

    private void saveServers () {
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString(AppSettings.servers, Utils.listToJsonString(servers));
        editor.commit();
    }

    protected void onResume() {
        super.onResume();
        reloadServers();
    }

    protected void onPause () {
        super.onPause();
        saveServers();
    }

    public void deleteServerRequested (View view) {
        SparseBooleanArray selected = lv.getCheckedItemPositions();

        for(int i = servers.size() - 1; i >= 0; i--){
            if(selected.get(i)){
                servers.remove(i);
            }
        }
        saveServers();
        reloadServers();
        lv.clearChoices();
    }

    public void addServerRequested (View view) {
        final EditText input = new EditText(this);

        new AlertDialog.Builder(ActivityManageServers.this)
                .setTitle(R.string.add_server_dialog_title)
                .setMessage(R.string.add_server_dialog_message)
                .setView(input)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value = input.getText().toString();
                        if(!servers.contains(value)) {
                            servers.add(value);
                            saveServers();
                            reloadServers();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show()
                .getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
}
