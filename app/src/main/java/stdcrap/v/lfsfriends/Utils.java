/*
Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

This file is part of LFSFriends.

        LFSFriends is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        LFSFriends is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with LFSFriends.  If not, see <http://www.gnu.org/licenses/>.
*/

package stdcrap.v.lfsfriends;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class Utils {
    public static void populateListFromJson (List<String> l, String js) {
        l.clear();
        if (js.length() == 0) {
            return;
        }
        try {
            JSONArray a = new JSONArray(js);
            for (int i = 0; i < a.length(); i++) {
                String s = a.getString(i);
                l.add(s);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("Utils::populateListFrom()", "failed parsing json: " + js);
        }
    }

    public static void populateListFromJson (List<String> l, JSONArray a) {
        l.clear();
        try {
            for (int i = 0; i < a.length(); i++) {
                String s = a.getString(i);
                l.add(s);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String listToJsonString (List<String> l) {
        JSONArray arr = new JSONArray();
        for (String s : l) {
            arr.put(s);
        }
        return arr.toString();
    }

    public static boolean containsCaseInsensitive(String s, List<String> l){
        for (String string : l){
            if (string.equalsIgnoreCase(s)){
                return true;
            }
        }
        return false;
    }
}
